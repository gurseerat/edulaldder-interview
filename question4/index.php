<!DOCTYPE HTML>
<html>
    <head>
        <title>
            Login
        </title>
        <meta name = "viewport" content = "width = device-width, initial-scale = 1">
        <link rel = "stylesheet" href = "https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <script src = "https://code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src = "https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
        
        <link rel="stylesheet" href="style.css">
    </head>
    <body>

<div data-role="page">
    <div data-role="content">
        <div class="container">
                <div id="my-wrapper">
                <form>
                    <input id="filterTable-input" data-type="search">
                </form>
                <table data-role="table" id="movie-table" data-filter="true" data-input="#filterTable-input" class="ui-responsive">
                    <thead>
                        <tr><td>State No.</td>
                        <td>State Name</td></tr>
                    </thead>
                    <tbody>
                    <?php
                                $con = mysqli_connect("localhost","root","","edu");
                                $sql = "SELECT StateId, StateName from state order by StateId asc";
                                if ($result=mysqli_query($con,$sql)){
                                    while ($row=mysqli_fetch_row($result)){
                                        echo "<tr><td>".$row[0]."</td><td>".$row[1]."</td></tr>";
                                    }
                                    mysqli_free_result($result);
                                }

                            mysqli_close($con);
                            ?>
                    </tbody>

                        
                    </div>
        </div> 
    </div>
</div>
    </body>
</html>